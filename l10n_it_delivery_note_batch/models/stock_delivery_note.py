# Copyright (c) 2020 Marco Colombo
# @author: Marco Colombo <marco.colombo@gmail.com>

from flectra import fields, models


class StockDeliveryNote(models.Model):
    _inherit = "stock.delivery.note"

    stock_picking_batch_id = fields.Many2one(
        "stock.picking.batch",
        string="Batch Picking",
        readonly=True,
    )
