# Flectra Community / l10n-italy

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[l10n_it_mis_reports_pl_bs](l10n_it_mis_reports_pl_bs/) | 2.0.1.0.0| Modelli "MIS Builder" per il conto economico e lo stato patrimoniale
[l10n_it_website_portal_fiscalcode](l10n_it_website_portal_fiscalcode/) | 2.0.1.0.0| Add fiscal code to details of frontend user
[l10n_it_abicab](l10n_it_abicab/) | 2.0.1.0.1| Base Bank ABI/CAB codes
[l10n_it_vat_statement_split_payment](l10n_it_vat_statement_split_payment/) | 2.0.1.0.2| Migliora la liquidazione dell'IVA tenendo in considerazione la scissione dei pagamenti
[l10n_it_central_journal](l10n_it_central_journal/) | 2.0.1.0.0| ITA - Libro giornale
[l10n_it_fatturapa_sale](l10n_it_fatturapa_sale/) | 2.0.1.0.1| Aggiunge alcuni dati per la fatturazione elettronica nell'ordine di vendita
[l10n_it_website_portal_ipa](l10n_it_website_portal_ipa/) | 2.0.1.0.0| Aggiunge l'indice PA (IPA) tra i dettagli dell'utente nel portale.
[l10n_it_vat_registries_split_payment](l10n_it_vat_registries_split_payment/) | 2.0.1.1.1| Modulo di congiunzione tra registri IVA e scissione dei pagamenti
[l10n_it_fiscal_payment_term](l10n_it_fiscal_payment_term/) | 2.0.1.0.0| Condizioni di pagamento delle fatture elettroniche
[l10n_it_fatturapa_out_sp](l10n_it_fatturapa_out_sp/) | 2.0.1.0.3| Scissione pagamenti in fatturapa
[l10n_it_delivery_note_batch](l10n_it_delivery_note_batch/) | 2.0.1.0.0| Crea i DDT partendo da gruppi di prelievi
[l10n_it_fatturapa_out_rc](l10n_it_fatturapa_out_rc/) | 2.0.1.0.5| Integrazione l10n_it_fatturapa_out e l10n_it_reverse_charge
[l10n_it_withholding_tax_reason](l10n_it_withholding_tax_reason/) | 2.0.1.0.2| ITA - Causali pagamento per ritenute d'acconto
[l10n_it_fatturapa_pec](l10n_it_fatturapa_pec/) | 2.0.1.0.3| Invio fatture elettroniche tramite PEC
[l10n_it_split_payment](l10n_it_split_payment/) | 2.0.1.0.4| Scissione pagamenti
[l10n_it_vat_payability](l10n_it_vat_payability/) | 2.0.1.0.2| ITA - Esigibilità IVA
[l10n_it_vat_registries](l10n_it_vat_registries/) | 2.0.1.0.5| ITA - Registri IVA
[l10n_it_fatturapa_in_rc](l10n_it_fatturapa_in_rc/) | 2.0.1.0.0| Modulo ponte tra e-fattura in acquisto e inversione contabile
[l10n_it_fatturapa_out_di](l10n_it_fatturapa_out_di/) | 2.0.1.0.2| Dichiarazioni d'intento in fatturapa
[l10n_it_pec](l10n_it_pec/) | 2.0.1.0.0| Aggiunge il campo email PEC al partner
[l10n_it_invoices_data_communication](l10n_it_invoices_data_communication/) | 2.0.1.0.1| Comunicazione dati fatture (c.d. "nuovo spesometro" o "esterometro")
[l10n_it_appointment_code](l10n_it_appointment_code/) | 2.0.1.0.0| Aggiunge la tabella dei codici carica da usare nelle dichiarazioni fiscali italiane
[l10n_it_fatturapa_out](l10n_it_fatturapa_out/) | 2.0.2.1.0| Emissione fatture elettroniche
[l10n_it_intrastat](l10n_it_intrastat/) | 2.0.1.0.2| Riclassificazione merci e servizi per dichiarazioni Intrastat
[l10n_it_reverse_charge](l10n_it_reverse_charge/) | 2.0.1.1.1| Inversione contabile
[l10n_it_account_tax_kind](l10n_it_account_tax_kind/) | 2.0.1.0.2| Gestione natura delle aliquote IVA
[l10n_it_payment_reason](l10n_it_payment_reason/) | 2.0.1.0.2| Aggiunge la tabella delle causali di pagamento da usare ad esempio nelle ritenute d'acconto
[account_vat_period_end_statement](account_vat_period_end_statement/) | 2.0.1.0.2| Allow to create the 'VAT Statement'.
[l10n_it_declaration_of_intent](l10n_it_declaration_of_intent/) | 2.0.1.1.1| Gestione dichiarazioni di intento
[l10n_it_pos_fiscalcode](l10n_it_pos_fiscalcode/) | 2.0.1.0.0| Gestione codice fiscale del cliente all'interno dell'interfaccia del POS
[l10n_it_account_stamp](l10n_it_account_stamp/) | 2.0.1.1.0| Gestione automatica dell'imposta di bollo
[l10n_it_fatturapa_in](l10n_it_fatturapa_in/) | 2.0.1.3.0| Ricezione fatture elettroniche
[l10n_it_account](l10n_it_account/) | 2.0.1.1.6| Modulo base usato come dipendenza di altri moduli contabili
[l10n_it_ricevute_bancarie](l10n_it_ricevute_bancarie/) | 2.0.1.1.1| Ricevute bancarie
[l10n_it_delivery_note_base](l10n_it_delivery_note_base/) | 2.0.1.0.0| Crea e gestisce tabelle principali per gestire i DDT
[l10n_it_fatturapa_in_purchase](l10n_it_fatturapa_in_purchase/) | 2.0.1.0.0| Modulo ponte tra ricezione fatture elettroniche e acquisti
[l10n_it_intrastat_statement](l10n_it_intrastat_statement/) | 2.0.1.1.1| Dichiarazione Intrastat per l'Agenzia delle Dogane
[l10n_it_fatturapa](l10n_it_fatturapa/) | 2.0.1.1.1| Fatture elettroniche
[l10n_it_fatturapa_export_zip](l10n_it_fatturapa_export_zip/) | 2.0.1.0.0| Permette di esportare in uno ZIP diversi file XML di fatture elettroniche
[l10n_it_ateco](l10n_it_ateco/) | 2.0.1.0.0| ITA - Codici Ateco
[l10n_it_delivery_note](l10n_it_delivery_note/) | 2.0.1.0.13| Crea, gestisce e fattura i DDT partendo dalle consegne
[l10n_it_rea](l10n_it_rea/) | 2.0.1.0.4| Gestisce i campi del Repertorio Economico Amministrativo
[l10n_it_withholding_tax](l10n_it_withholding_tax/) | 2.0.1.1.3| ITA - Ritenute d'acconto
[l10n_it_fatturapa_out_stamp](l10n_it_fatturapa_out_stamp/) | 2.0.1.0.0| Modulo ponte tra emissione fatture elettroniche e imposta di bollo
[l10n_it_sdi_channel](l10n_it_sdi_channel/) | 2.0.1.0.1| Aggiunge il canale di invio/ricezione dei file XML attraverso lo SdI
[l10n_it_fiscal_document_type](l10n_it_fiscal_document_type/) | 2.0.1.0.6| ITA - Tipi di documento fiscale per dichiarativi
[l10n_it_invoices_data_communication_fatturapa](l10n_it_invoices_data_communication_fatturapa/) | 2.0.1.0.0| Integrazione fatturazione elettronica e comunicazione dati fatture (c.d. "nuovo spesometro")
[l10n_it_delivery_note_customer_code](l10n_it_delivery_note_customer_code/) | 2.0.1.0.0| Product Customer code for delivery note
[l10n_it_withholding_tax_payment](l10n_it_withholding_tax_payment/) | 2.0.1.0.2| Gestisce le ritenute sulle fatture e sui pagamenti
[l10n_it_fiscalcode](l10n_it_fiscalcode/) | 2.0.1.0.2| ITA - Codice fiscale
[l10n_it_vat_statement_communication](l10n_it_vat_statement_communication/) | 2.0.1.0.0| Comunicazione liquidazione IVA ed esportazione file xmlconforme alle specifiche dell'Agenzia delle Entrate
[l10n_it_ipa](l10n_it_ipa/) | 2.0.1.0.2| ITA - Codice IPA
[l10n_it_delivery_note_order_link](l10n_it_delivery_note_order_link/) | 2.0.1.0.1| Crea collegamento tra i DDT e ordine di vendita/acquisto


