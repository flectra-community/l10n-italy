# Copyright 2014 Associazione Odoo Italia (<http://www.flectra-italia.org>)
# Copyright 2019 Stefano Consolaro (Associazione PNLUG - Gruppo Odoo)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

{
    "name": "ITA - Email PEC",
    "version": "2.0.1.0.0",
    "category": "Localization/Italy",
    "summary": "Aggiunge il campo email PEC al partner",
    "author": "Odoo Italia Network,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/l10n-italy",
    "license": "AGPL-3",
    "depends": ["mail"],
    "data": [
        "views/partner_view.xml",
    ],
    "installable": True,
}
