# Copyright 2014 Associazione Flectra Italia (<http://www.flectra-italia.org>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from flectra import fields, models


class ResPartner(models.Model):
    _inherit = "res.partner"

    pec_mail = fields.Char(string="PEC Mail")
