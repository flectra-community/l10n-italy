# Copyright 2015 Associazione Odoo Italia (<http://www.flectra-italia.org>)
# Copyright 2016 Davide Corio (Abstract)
# Copyright 2018 Sergio Zanchetta (Associazione PNLUG - Gruppo Odoo)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "ITA - Codici bancari ABI/CAB",
    "version": "2.0.1.0.1",
    "category": "Localization/Italy",
    "development_status": "Production/Stable",
    "summary": "Base Bank ABI/CAB codes",
    "author": "Odoo Italia Network, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/l10n-italy",
    "license": "AGPL-3",
    "maintainers": ["Borruso"],
    "depends": ["account"],
    "data": ["views/abicab_view.xml"],
    "installable": True,
}
